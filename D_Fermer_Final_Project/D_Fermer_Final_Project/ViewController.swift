//
//  ViewController.swift
//  D_Fermer_Final_Project
//
//  Created by David Fermer on 8/14/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import MapKit

// Global Scope Identifier
let annotationIdentifer = "annotationReuse"

// MARK: - CLLOcationManagerDelegate
extension ViewController: CLLocationManagerDelegate {
	func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		if status == .AuthorizedWhenInUse {
			locationManager.startUpdatingLocation()
		}
	}
	
	func locationManager(manager: CLLocationManager!, didFinishDeferredUpdatesWithError error: NSError!) {
		//TODO: - Alert Message showing error.
		return
	}
}

// MARK: - MKMapViewDelegate
extension ViewController: MKMapViewDelegate {
	func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
		if CLLocationManager.authorizationStatus() != .AuthorizedWhenInUse {
			return
		}
		
		let span = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
		let region = MKCoordinateRegion(center: userLocation.coordinate, span: span)
		
		mapView.setRegion(region, animated: true)
	}
	
	func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
		
		if annotation is MKUserLocation {
			return nil
		}
		
		if annotation.coordinate.latitude == 0.0 && annotation.coordinate.latitude == 0.0 {
			return nil
		}
		
		var test = MKAnnotationView()
		test.annotation = annotation
		
		if let annotationTitle = annotation.title {
			switch annotationTitle {
			case "Black Car":
				annotationImage = UIImage(named: "blackCar")
			case "Blue Car":
				annotationImage = UIImage(named: "blueCar")
			default:
				annotationImage = UIImage(named: "key")
			}
		}
		var testImage = annotationImage
		
		test.image = testImage
		return test
	}
}

// MARK: - ViewController
class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate{
	//Outlets
	@IBOutlet weak var pinTypeSelector: UISegmentedControl!
	@IBOutlet weak var mapView: MKMapView!
	//Variables
	
	let userDefaults = NSUserDefaults(suiteName: "group.DavidFermerMDF2FinalProject")
	var locations : [Location] = []
	var annotations : [MKAnnotation] = []
	
	let locationManager = CLLocationManager()
	var needUpdate = false
	var annotationImage : UIImage?
	var previouslySetPin = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		locationManager.delegate = self
		
		let status = CLLocationManager.authorizationStatus()
		
		switch status {
		case .NotDetermined:
			locationManager.requestWhenInUseAuthorization()
		case .AuthorizedWhenInUse:
			locationManager.startUpdatingLocation()
		default:
			// TODO: - create alert
			break
		}
		
		mapView.showsUserLocation = true
		buildPreviousPins()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	func buildPreviousPins(){
		
		//If blackCar Pin was set previously restore it from defaults if not set it to a fake pin.
		if let blackCarLatitude = userDefaults?.doubleForKey("latitudeBlackCar"), blackCarLongitude = userDefaults?.doubleForKey("longitudeBlackCar") {
			let annotation = MKPointAnnotation()
			annotation.coordinate = CLLocationCoordinate2D(latitude: blackCarLatitude, longitude: blackCarLongitude)
			annotation.title = "Black Car"
			annotations.append(annotation)
			mapView.addAnnotation(annotation)
		} else {
			let annotation = MKPointAnnotation()
			annotation.coordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
			annotations.append(annotation)
		}
		
		//If blueCar Pin was set previously restore it from defaults if not set it to a fake pin.
		if let blueCarLatitude = userDefaults?.doubleForKey("latitudeBlueCar"), blueCarLongitude = userDefaults?.doubleForKey("longitudeBlueCar") {
			let annotation = MKPointAnnotation()
			annotation.coordinate = CLLocationCoordinate2D(latitude: blueCarLatitude, longitude: blueCarLongitude)
			annotation.title = "Blue Car"
			annotations.append(annotation)
			mapView.addAnnotation(annotation)
		} else {
			let annotation = MKPointAnnotation()
			annotation.coordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
			annotations.append(annotation)
		}
		
		//If Key Pin was set previously restore it from defaults if not set it to a fake pin.
		if let keyLatitude = userDefaults?.doubleForKey("latitudeKey"), keyLongitude = userDefaults?.doubleForKey("longitudeKey") {
			let annotation = MKPointAnnotation()
			annotation.coordinate = CLLocationCoordinate2D(latitude: keyLatitude, longitude: keyLongitude)
			annotation.title = "Key"
			annotations.append(annotation)
			mapView.addAnnotation(annotation)
		} else {
			let annotation = MKPointAnnotation()
			annotation.coordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
			annotations.append(annotation)
		}
	}
	
	func removePin(pinTitle: String){
		var allPins = mapView.annotations
		for pin in allPins {
			if pin.title == pinTitle{
				mapView.removeAnnotation(pin as! MKAnnotation)
			}
		}
	}
	
	@IBAction func markThisLocationButton(sender: UIButton) {
		let annotation = MKPointAnnotation();
		annotation.coordinate = mapView.userLocation.coordinate
		
		
		var latitudeKey = ""
		var longitudeKey = ""
		var lastUsed = 0
		
		switch pinTypeSelector.selectedSegmentIndex {
		case 0 :
			annotationImage = UIImage(named: "blackCar")
			latitudeKey = "latitudeBlackCar"
			longitudeKey = "longitudeBlackCar"
			userDefaults?.setInteger(0, forKey: "lastUsed")
			annotation.title = "Black Car"
			removePin("Black Car")
		case 1 :
			annotationImage = UIImage(named: "blueCar")
			latitudeKey = "latitudeBlueCar"
			longitudeKey = "longitudeBlueCar"
			userDefaults?.setInteger(1, forKey: "lastUsed")
			annotation.title = "Blue Car"
			removePin("Blue Car")
		default:
			annotationImage = UIImage(named: "key")
			latitudeKey = "latitudeKey"
			longitudeKey = "longitudeKey"
			userDefaults?.setInteger(2, forKey: "lastUsed")
			annotation.title = "Key"
			removePin("Key")
		}
		
		annotations[lastUsed] = annotation
		
		mapView.addAnnotation(annotation)
		
		userDefaults?.setDouble(Double(annotation.coordinate.latitude), forKey: latitudeKey)
		userDefaults?.setDouble(Double(annotation.coordinate.longitude), forKey: longitudeKey)
		
		userDefaults?.synchronize()
		println(userDefaults?.integerForKey("lastUsed"))
	}
	
	
}

