//
//  Location.swift
//  mark_and_go
//
//  Created by David Fermer on 8/5/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import Foundation

class Location {
	var latitude: Double
	var longitude: Double
	
	init(latitude: Double, longitude: Double) {
		self.latitude = latitude
		self.longitude = longitude
	}
}