//
//  SelectorInterfaceController.swift
//  D_Fermer_Final_Project
//
//  Created by David Fermer on 8/20/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import WatchKit
import Foundation


class SelectorInterfaceController: WKInterfaceController {
	
	//set User Defaults
	let userDefaults = NSUserDefaults(suiteName: "group.DavidFermerMDF2FinalProject")
	
	//set row images
	let images = [UIImage(named: "blackCar"), UIImage(named: "blueCar"), UIImage(named: "key")]
	//set row labels text
	let labels = ["Black Car", "Blue Car", "Key"]
	
	@IBOutlet weak var table: WKInterfaceTable!
	
 
	override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
		
		self.table.setNumberOfRows(3, withRowType: "tableClass")
        
        // Configure interface objects here.
	
		for row in 0..<self.table.numberOfRows {
			let currentrow = self.table.rowControllerAtIndex(row) as! tableClass
			
			currentrow.rowImage.setImage(images[row])
			currentrow.rowLabel.setText(labels[row])
		}
	
	}

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
		
    }
	
	override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
		userDefaults?.setInteger(rowIndex, forKey: "lastUsed")
		popController()
	}

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
