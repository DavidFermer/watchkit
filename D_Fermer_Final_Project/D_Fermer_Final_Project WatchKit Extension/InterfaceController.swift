//
//  InterfaceController.swift
//  D_Fermer_Final_Project WatchKit Extension
//
//  Created by David Fermer on 8/14/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
	
	//Variables
	//set user defaults
	let userDefaults = NSUserDefaults(suiteName: "group.DavidFermerMDF2FinalProject")
	//Key Names for NSUserDefaults
	//Blue Car Key Names
	let blueCarLatitudeKeyName = "latitudeBlueCar"
	let blueCarLongitudeKeyName = "longitudeBlueCar"
	//Black Car Key Names
	let blackCarLatitudeKeyName = "latitudeBlackCar"
	let blackCarLongitudeKeyName = "longitudeBlackCar"
	//Key, Key Names
	let keyLatitudeKeyName = "latitudeKey"
	let keyLongitudeKeyName = "longitudeKey"
	//Last used Key Names - Last used is the last type of pin that was dropped.
	let lastUsedKeyName = "lastUsed"
	var lastUsedLatKey = ""
	var lastUsedLonKey = ""
	var imageForLastPin : UIImage?
	
	@IBOutlet weak var mapView: WKInterfaceMap!
	
	override func awakeWithContext(context: AnyObject?) {
		super.awakeWithContext(context)
	}
	
	override func willActivate() {
		// This method is called when watch view controller is about to be visible to user
		super.willActivate()
		mapView.removeAllAnnotations()
		
		userDefaults?.synchronize()
		println(userDefaults?.integerForKey("lastUsed"))
		
		let coordinateSpan = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
		if let lastUsedKey = userDefaults?.integerForKey(lastUsedKeyName) {
			println(lastUsedKey)
			switch lastUsedKey {
			case 0:
				lastUsedLatKey = blackCarLatitudeKeyName
				lastUsedLonKey = blackCarLongitudeKeyName
				imageForLastPin = UIImage(named: "blackCar")
			case 1:
				lastUsedLatKey = blueCarLatitudeKeyName
				lastUsedLonKey = blueCarLongitudeKeyName
				imageForLastPin = UIImage(named: "blueCar")
			default:
				lastUsedLatKey = keyLatitudeKeyName
				lastUsedLonKey = keyLongitudeKeyName
				imageForLastPin = UIImage(named: "key")
			}
		}

	
	var locationLong = userDefaults?.doubleForKey(lastUsedLonKey)
	var locationLat = userDefaults?.doubleForKey(lastUsedLatKey)
	
	let location = CLLocationCoordinate2D(latitude: locationLat!, longitude: locationLong!)
	
	mapView.setVisibleMapRect(MKMapRect(origin: MKMapPointForCoordinate(location),
	size: MKMapSize(width: 0.5, height: 0.5)))
	mapView.setRegion(MKCoordinateRegion(center: location, span: coordinateSpan))
	
	if let locationLong = locationLong, locationLat = locationLat {
		let	annotation = CLLocationCoordinate2D(latitude: locationLat, longitude: locationLong)
		if let imageForLastPin = imageForLastPin {
			mapView.addAnnotation(annotation, withImage: imageForLastPin, centerOffset: CGPoint(x: 0, y: 0))
		}
	}
}


override func didDeactivate() {
	// This method is called when watch view controller is no longer visible
	super.didDeactivate()
}





}
